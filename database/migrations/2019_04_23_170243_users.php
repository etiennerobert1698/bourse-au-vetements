<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->onDelete('cascade');
            $table->string('tel', 10);
            $table->string('nom', 25);
            $table->string('prenom', 25);
            $table->string('email', 190)->unique();
            $table->string('password', 255);
            $table->string('ville', 25);
            $table->string('adresse', 255);
            $table->string('cp', 5);
            $table->integer('droit_id')->length('10')->unsigned()->default('3');
            $table->timestamps();

            
        });
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('droit_id')->references('id')->on('droits');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
