<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Articles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->onDelete('cascade');
            $table->integer('liste_id')->length('10')->unsigned();
            $table->integer('taille_id')->length('10')->unsigned();
            $table->integer('type_id')->length('10')->unsigned();

            $table->string('description', 400);
            $table->integer('prix');

            
        });
        Schema::table('articles', function (Blueprint $table) {
            $table->foreign('liste_id')->references('id')->on('listes');
            $table->foreign('type_id')->references('id')->on('types');
            $table->foreign('taille_id')->references('id')->on('tailles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
