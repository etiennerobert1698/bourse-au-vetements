<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as Controller;
use Illuminate\Http\Request;
use App\models\User;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function add(Request $request){
		$this->validate($request, [
			'email' => 'required|email|unique:users',
        	'nom' => 'required',
        	'prenom' => 'required',
        	'adresse' => 'required',
        	'ville' => 'required',
        	'cp' => 'required',
        	'tel' => 'required',
            'password' =>'required'
    	]);

        $attributes = $request->input();
        $attributes['password'] = Hash::make($attributes['password']);
    	
        $user = new User($attributes);
        $user->save();

        return json_encode($user);
    }

    public function findAll(){
        $users = User::all();
        foreach ($users as $user) {
            $user->setAttributes();
        }
    	return json_encode($users);
    }
}
