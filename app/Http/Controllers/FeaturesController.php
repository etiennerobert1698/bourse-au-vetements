<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as Controller;
use Illuminate\Http\Request;


use App\models\Type;
use App\models\Taille;



class FeaturesController extends Controller
{
    public function getTypes(){
        return json_encode(Type::all());
    }
    public function getTailles(){
        return json_encode(Taille::all());
    }
}
