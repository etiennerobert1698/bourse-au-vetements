<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as Controller;
use Illuminate\Http\Request;

use App\models\User;
use App\models\Droit;
use App\models\Liste;
use App\models\Article;
use App\models\Type;
use App\models\Taille;
use App\models\Bourse;


class ListesController extends Controller
{
    public function newListe(Request $request){

        $id_bourse = Bourse::getActualBourse()->id;
        if($request->input('auth')->listes()->where('bourse_id', '=', $id_bourse)->count()  < 3){
            
            $liste = new Liste(array('user_id' => $request->input('auth')->id, 'bourse_id' => $id_bourse));
            

            if($articles = $this->addAllArticles($liste, $request)){
                $liste->save();
                $liste->articles()->saveMany($articles);
                return response()->json(['success' => 'Liste ajoute']);

            }
            else{
                return response()->json(['error' => 'Nombre maximum d\'articles (26) ou 0 article']);
            }


        }
         else{
            return response()->json([
            'error' => 'Nombre maximum de listes (3)'
            ]);
         }
    }
    private function addAllArticles(Liste $liste, Request $request){
    //26 places dans une liste mais $request->input() contient aussi l'utilisateur connecte
        if(sizeof($request->input()) <= 27){
            
            $articles = array();

            for ($i = 0; $i < sizeof($request->input()) - 1; $i++) {
                $this->validate($request, [
                    $i.'.type.id' => 'required|exists:types,id',
                    $i.'.taille.id' => 'required|exists:tailles,id',
                    $i.'.prix' => 'required|integer',
                    $i.'.description' => 'required'
           
                ]);
                $a = new Article($request->input($i));
                $a->type_id = $request->input($i)['type']['id'];
                $a->taille_id = $request->input($i)['taille']['id'];
                $articles[] = $a;
            }
            return $articles;
        }
        else{
            return false;
        }
          
    }
    public function delete(int $id_liste, Request $request){
        if($l = $request->input('auth')->listes->where('id', '=', $id_liste)->first()){
            $l->articles()->delete();
            $l->delete();
            return response()->json(['success' => 'Liste supprime']);
        }
        return response()->json(['error' => 'Vous ne possedez pas cette liste']);
    }
    public function findAll(){
        $listes = Liste::all();
        foreach ($listes as $liste) {
            $liste->setAttributes();
            foreach ($liste->articles as $article) {
                $article->setAttributes();
            }
        }
        return json_encode($listes);
    }
    public function getAuthLists(Request $request){
        if($listes = $request->input('auth')->listes){
            foreach ($listes as $liste) {
                $liste->setAttributes();
                foreach ($liste->articles as $article) {
                    $article->setAttributes();
                }
            }
            return json_encode($listes);
        }
        return 0;
    }
}
