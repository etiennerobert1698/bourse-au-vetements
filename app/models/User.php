<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;



class User extends Model
{
   

	protected $table = 'users';
	protected $fillable = array('nom', 'prenom', 'email', 'adresse', 'ville',  'tel', 'ville', 'cp','id_droit', 'password');
	protected $hidden = array('pivot', 'created_at', 'updated_at', 'password', 'droit_id' );

	public function listes(){
		return $this->hasMany('App\models\Liste');
	}
	public function droit(){
		return $this->belongsTo('App\models\Droit');
	}
	
	public function setAttributes(){
		$this->droit;
	}


}
