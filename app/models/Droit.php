<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;


class Droit extends Model
{
   
    
	protected $table = 'droits';
	public $timestamps = false;
	public $fillable =  array('libelle');
	protected $hidden = array('pivot');

	
	



}
