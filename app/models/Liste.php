<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;



class Liste extends Model
{
   
    
	protected $table = 'listes';
	public $timestamps = false;
	protected $fillable =  array('user_id', 'bourse_id');
	protected $hidden = array('pivot', 'created_at', 'updated_at','user_id', 'bourse_id');

	public function articles(){
		return $this->hasMany('App\models\Article');
	}

	public function bourse(){
		return $this->belongsTo('App\models\Bourse');
	}
	public function user(){
		return $this->belongsTo('App\models\User');
	}
	public function setAttributes(){
		$this->bourse;
		$this->articles;
		$this->user;
	}
	



}
