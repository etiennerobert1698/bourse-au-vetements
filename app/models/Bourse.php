<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;


class Bourse extends Model
{
   
    
	protected $table = 'bourses';
	protected $fillable = array('date_depot', 'date_vente', 'date_remise');
	protected $hidden = array('pivot', 'created_at', 'updated_at');

	public function listes(){
		return $this->hasMany('App\models\Liste');
	}

	public static function getActualBourse(){
		        
    //A MODIFIE $actualBourse doit prendre la bourse en cours
        
		$actualBourse = new Bourse();
		$actualBourse->id = 1;
		return $actualBourse;
	}


	



}
