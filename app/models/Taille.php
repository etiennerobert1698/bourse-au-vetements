<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;


class Taille extends Model
{
   
    
	protected $table = 'tailles';
	public $timestamps = false;
	public $fillable =  array('libelle_taille' );
	protected $hidden = array('pivot', 'created_at', 'updated_at' );

	
	



}
