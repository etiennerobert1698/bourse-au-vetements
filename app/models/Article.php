<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;


class Article extends Model
{
   
    
	protected $table = 'articles';
	public $timestamps = false;
	public $fillable =  array('type_id', 'taille_id', 'type_id', 'description', 'prix' );
	protected $hidden = array('pivot', 'created_at', 'updated_at', 'liste_id','type_id', 'taille_id' );
	
	public function type(){
		return $this->belongsTo('App\models\Type');
	}
	public function taille(){
		return $this->belongsTo('App\models\Taille');
	}

	public function setAttributes(){
		$this->type;
		$this->taille;
	}


}
