<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->post('auth/login', 'AuthController@authenticate');
$router->post('users', 'UsersController@add');

// --- Features ---
$router->get('types', 'FeaturesController@getTypes');
$router->get('tailles', 'FeaturesController@getTailles');

 
$router->group(
    ['middleware' => 'jwt.auth'], 
    function() use ($router) {
      
    	$router->delete('listes/{id_liste:[0-9]+}', 'ListesController@delete');
    	$router->post('listes', 'ListesController@newListe');
    	$router->get('listes', 'ListesController@getAuthLists');



// 				---- Partie Administration ----
        $router->get('users/all', 'UsersController@findAll');
        $router->get('listes/all', 'ListesController@findAll');
	}
);


